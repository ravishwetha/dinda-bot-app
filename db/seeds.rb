# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Robot.create(name: "Gigabit", slogan: "Only my army of killer robots understands me..", active_status:"yes", weapon:"tester")
Robot.create(name: "Rob Bitt", slogan: "I am not a simple Computer.",active_status:"yes", weapon:"tester")
Robot.create(name: "Screwie", slogan: "I'm still in beta..",active_status:"yes", weapon:"tester")
Robot.create(name: "Brobot", slogan: "Optimus Primates RULE!",active_status:"yes", weapon:"tester")
Robot.create(name: "Databorg", slogan: "I am borg.",active_status:"yes", weapon:"tester")